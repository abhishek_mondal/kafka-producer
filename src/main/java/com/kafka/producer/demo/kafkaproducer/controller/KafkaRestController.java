package com.kafka.producer.demo.kafkaproducer.controller;

import com.kafka.producer.demo.kafkaproducer.model.Employee;
import com.kafka.producer.demo.kafkaproducer.service.KafkaMessagePublisher;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/kafka-producer")
public class KafkaRestController {

    @Autowired
    private KafkaMessagePublisher publisher;

    @GetMapping(value = "/publish")
    public ResponseEntity<String> publishMessage(@RequestParam String message) {
        publisher.sendMessage(message);
        return new ResponseEntity("Message sent successfully", HttpStatus.OK);
    }

    @PostMapping(value = "/sendEmployeeData", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> sendEmployeeData(@RequestBody Employee employee) {
        //log.info("Employee :: " +employee);
        if(!StringUtils.isEmpty(employee))
        publisher.sendEmployeeData(employee);
        return new ResponseEntity("Employee Data sent successfully.", HttpStatus.OK);
    }

}
