package com.kafka.producer.demo.kafkaproducer.service;

import com.kafka.producer.demo.kafkaproducer.model.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.util.UUID;

@Service
public class KafkaMessagePublisher {

    private static Logger LOGGER = LoggerFactory.getLogger(KafkaMessagePublisher.class);

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    @Qualifier("CustomKafkaTemplate")
    private KafkaTemplate<String, Employee> customKafkaTemplate;

    String topicName = "testTopic";

    public void sendMessage(String message) {
        //Fire and Forgot
        //kafkaTemplate.send(topicName, message);
        //Fire & Expect Result
        ListenableFuture<SendResult<String, String>> future =
                kafkaTemplate.send(topicName, UUID.randomUUID().toString(), message);

        future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {

            @Override
            public void onSuccess(SendResult<String, String> result) {
                LOGGER.info("Sent message=[" + message +
                        "] with offset=[" + result.getRecordMetadata().offset() + "]");
            }

            @Override
            public void onFailure(Throwable ex) {
                LOGGER.info("Unable to send message=["
                        + message + "] due to : " + ex.getMessage());
            }
        });
    }

    public void sendEmployeeData(Employee emp) {

        customKafkaTemplate.send(topicName, emp);
    }

}
